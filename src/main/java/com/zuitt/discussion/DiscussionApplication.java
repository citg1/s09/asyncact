package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.Optional;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	private ArrayList<String> enrollees = new ArrayList<String>();
	public DiscussionApplication() {enrollees = new ArrayList<String>();}

	@GetMapping("/enroll")
	public String enrollUser(@RequestParam("user") String username) {
		enrollees.add(username);
		return "User " + username + " enrolled successfully!";
	}

	@GetMapping("/getEnrollees")
	public String getEnrollees() {
		return enrollees.toString();
	}

	@GetMapping("/nameage")
	public String getNameAndAge(@RequestParam("name") String name, @RequestParam("age") int age) {
		return "Hello " + name + "! My age is " + age + ".";
	}

	@GetMapping("/courses/{id}")
	public String getCourseDetails(@PathVariable String id) {
		if (id.equals("java101")) {
			return "Java 101, MWF 8:00AM-11:00AM, PHP 3000.00";
		}
		if (id.equals("sql101")) {
			return "SQL 101, TTH 1:00PM-4:00PM, PHP 2000.00";
		}
		if (id.equals("javaee101")) {
			return "java EE 101, MWF 1:00PM-4:00PM, PHP 3500.00";
		}
		else {
			return "Course not found";
		}
	}
	@GetMapping("/welcome")
	public String welcomeUser(@RequestParam("user") String user, @RequestParam("role") String role) {
		if (role.equals("admin")) {
			return "Welcome back to the class portal, Admin " + user + "!";
		}
		if (role.equals("teacher")) {
			return "Thank you for logging in, Teacher " + user + "!";
		}
		if (role.equals("student")) {
			return "Welcome to the class portal " + user + "!";
		}
		else {
			return "Role out of range!";
		}
	}
	ArrayList<Student> students = new ArrayList<>();

	@GetMapping("/register")
	public String registerStudent(@RequestParam("id") int id, @RequestParam("name") String name, @RequestParam("course") String course) {
		Student student = new Student(id, name, course);
		students.add(student);
		return id + " your id number is registered on the system!";
	}

	@GetMapping("/getStudents")
	public ArrayList<Student> getStudents() {
		return students;
	}


	@GetMapping("/account/{id}")
	public String getAccount(@PathVariable int id) {
		Optional<Student> student = students.stream()
				.filter(s -> s.getId() == id)
				.findFirst();

		if (student.isPresent()) {
			return "Welcome back " + student.get().getName() + "! You are currently enrolled in " + student.get().getCourse();
		} else {
			return "Your provided " + id + " is not found in the system!";
		}
	}

	private static class Student {
		private final int id;
		private final String name;
		private final String course;

		public Student(int id, String name, String course) {
			this.id = id;
			this.name = name;
			this.course = course;
		}

		public int getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public String getCourse() {
			return course;
		}
	}
}


